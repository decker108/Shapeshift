﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class VictoryConditions : MonoBehaviour {

	private HashSet<int> objIdsInZone = new HashSet<int>(); // VObjs = Victory Objects

	[Range(1, 20)] 
	public int neededVObjsToWin = 1;

	void Start () {
	}

	bool allVictoryObjectsInZone () {
		return neededVObjsToWin == objIdsInZone.Count;
	}
	
	void Update () {
		if (allVictoryObjectsInZone()) {
			Debug.Log ("Victory!");
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.tag.Equals("VictoryObject") && !objIdsInZone.Contains(other.gameObject.GetInstanceID())) {
			objIdsInZone.Add (other.gameObject.GetInstanceID());
		}
		//Debug.Log("Collided with " + other.tag + " w id " + other.GetInstanceID());
	}
}
